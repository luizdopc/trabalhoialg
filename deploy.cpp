#include <bits/stdc++.h>
using namespace std;

struct Filosofos{
	string nome;
	int anoNascimento;
	string principaisIdeias;
	string livroMaisConhecido;
	bool registroValido;
};

void realocar(Filosofos * &filosofos, int &qtdMaxima, int &qtdAtual){
	qtdMaxima += 2;
	Filosofos *aux = new Filosofos[qtdMaxima];

	for(int i=0; i<qtdAtual; i++){
		aux[i] = filosofos[i];
	}

	delete[] filosofos;

	filosofos = aux;
	cout << "memoria ajustada "<<qtdMaxima<<endl;
}

void incluir(Filosofos * &filosofos, int &qtdAtual) {
	getline(cin, filosofos[qtdAtual].nome);
	cin>>filosofos[qtdAtual].anoNascimento;
	cin.ignore();
	getline(cin, filosofos[qtdAtual].principaisIdeias);
	getline(cin, filosofos[qtdAtual].livroMaisConhecido);
	filosofos[qtdAtual].registroValido = true;
	qtdAtual++;
}

void excluir(Filosofos filosofos[], int qtdAtual){
	string aux;
	getline(cin, aux);
	
	int i=0;
	while(filosofos[i].nome != aux && i<=qtdAtual){
		i++;
	}
	
	if(i > qtdAtual){
		cout<<"nao existe"<<endl;
	}else{
		filosofos[i].registroValido = false;
	}
}

void alterar(Filosofos filosofos[], int qtdAtual){
	string aux;
	getline(cin, aux);
	
	int i=0;
	while(filosofos[i].nome != aux && i<=qtdAtual){
		i++;
	}

	if(i > qtdAtual){
		cout<<"nao existe"<<endl;
	}else{
		cin>>filosofos[i].anoNascimento;
		cin.ignore();
		getline(cin, filosofos[i].principaisIdeias);
		getline(cin, filosofos[i].livroMaisConhecido);
	}

}

void listar(Filosofos *filosofos, int qtdAtual){
	for(int i=0; i<qtdAtual; i++){
		if(filosofos[i].registroValido){
			cout<<filosofos[i].nome<<endl<<filosofos[i].anoNascimento<<endl<<filosofos[i].principaisIdeias<<endl<<filosofos[i].livroMaisConhecido<<endl;
		}		
	}
}



int main(){
	int qtdMaxima = 2;

	Filosofos *filosofos = new Filosofos[qtdMaxima];

	int qtdAtual = 0;
	
	int op;
	do{
		cin>>op;
		cin.ignore();

		if(op == 1){
			if(qtdAtual == qtdMaxima){
				realocar(filosofos, qtdMaxima, qtdAtual);
			}
			incluir(filosofos, qtdAtual);
		}else{
			if(op == 2){
				excluir(filosofos, qtdAtual);	
			}else{
				if(op == 3){
					listar(filosofos, qtdAtual);
				}else{
					if(op == 4){
						alterar(filosofos, qtdAtual);
					}
				}	
			}
		}
	}while(op != 5);
	return 0;
}
